
(function(l, r) { if (l.getElementById('livereloadscript')) return; r = l.createElement('script'); r.async = 1; r.src = '//' + (window.location.host || 'localhost').split(':')[0] + ':35729/livereload.js?snipver=1'; r.id = 'livereloadscript'; l.getElementsByTagName('head')[0].appendChild(r) })(window.document);
var app = (function () {
    'use strict';

    function noop() { }
    function add_location(element, file, line, column, char) {
        element.__svelte_meta = {
            loc: { file, line, column, char }
        };
    }
    function run(fn) {
        return fn();
    }
    function blank_object() {
        return Object.create(null);
    }
    function run_all(fns) {
        fns.forEach(run);
    }
    function is_function(thing) {
        return typeof thing === 'function';
    }
    function safe_not_equal(a, b) {
        return a != a ? b == b : a !== b || ((a && typeof a === 'object') || typeof a === 'function');
    }
    function is_empty(obj) {
        return Object.keys(obj).length === 0;
    }

    function append(target, node) {
        target.appendChild(node);
    }
    function insert(target, node, anchor) {
        target.insertBefore(node, anchor || null);
    }
    function detach(node) {
        node.parentNode.removeChild(node);
    }
    function destroy_each(iterations, detaching) {
        for (let i = 0; i < iterations.length; i += 1) {
            if (iterations[i])
                iterations[i].d(detaching);
        }
    }
    function element(name) {
        return document.createElement(name);
    }
    function text(data) {
        return document.createTextNode(data);
    }
    function space() {
        return text(' ');
    }
    function empty() {
        return text('');
    }
    function listen(node, event, handler, options) {
        node.addEventListener(event, handler, options);
        return () => node.removeEventListener(event, handler, options);
    }
    function attr(node, attribute, value) {
        if (value == null)
            node.removeAttribute(attribute);
        else if (node.getAttribute(attribute) !== value)
            node.setAttribute(attribute, value);
    }
    function children(element) {
        return Array.from(element.childNodes);
    }
    function custom_event(type, detail) {
        const e = document.createEvent('CustomEvent');
        e.initCustomEvent(type, false, false, detail);
        return e;
    }
    function attribute_to_object(attributes) {
        const result = {};
        for (const attribute of attributes) {
            result[attribute.name] = attribute.value;
        }
        return result;
    }

    let current_component;
    function set_current_component(component) {
        current_component = component;
    }
    function get_current_component() {
        if (!current_component)
            throw new Error('Function called outside component initialization');
        return current_component;
    }
    function onMount(fn) {
        get_current_component().$$.on_mount.push(fn);
    }
    function afterUpdate(fn) {
        get_current_component().$$.after_update.push(fn);
    }

    const dirty_components = [];
    const binding_callbacks = [];
    const render_callbacks = [];
    const flush_callbacks = [];
    const resolved_promise = Promise.resolve();
    let update_scheduled = false;
    function schedule_update() {
        if (!update_scheduled) {
            update_scheduled = true;
            resolved_promise.then(flush);
        }
    }
    function add_render_callback(fn) {
        render_callbacks.push(fn);
    }
    let flushing = false;
    const seen_callbacks = new Set();
    function flush() {
        if (flushing)
            return;
        flushing = true;
        do {
            // first, call beforeUpdate functions
            // and update components
            for (let i = 0; i < dirty_components.length; i += 1) {
                const component = dirty_components[i];
                set_current_component(component);
                update(component.$$);
            }
            set_current_component(null);
            dirty_components.length = 0;
            while (binding_callbacks.length)
                binding_callbacks.pop()();
            // then, once components are updated, call
            // afterUpdate functions. This may cause
            // subsequent updates...
            for (let i = 0; i < render_callbacks.length; i += 1) {
                const callback = render_callbacks[i];
                if (!seen_callbacks.has(callback)) {
                    // ...so guard against infinite loops
                    seen_callbacks.add(callback);
                    callback();
                }
            }
            render_callbacks.length = 0;
        } while (dirty_components.length);
        while (flush_callbacks.length) {
            flush_callbacks.pop()();
        }
        update_scheduled = false;
        flushing = false;
        seen_callbacks.clear();
    }
    function update($$) {
        if ($$.fragment !== null) {
            $$.update();
            run_all($$.before_update);
            const dirty = $$.dirty;
            $$.dirty = [-1];
            $$.fragment && $$.fragment.p($$.ctx, dirty);
            $$.after_update.forEach(add_render_callback);
        }
    }
    const outroing = new Set();
    function transition_in(block, local) {
        if (block && block.i) {
            outroing.delete(block);
            block.i(local);
        }
    }
    function mount_component(component, target, anchor, customElement) {
        const { fragment, on_mount, on_destroy, after_update } = component.$$;
        fragment && fragment.m(target, anchor);
        if (!customElement) {
            // onMount happens before the initial afterUpdate
            add_render_callback(() => {
                const new_on_destroy = on_mount.map(run).filter(is_function);
                if (on_destroy) {
                    on_destroy.push(...new_on_destroy);
                }
                else {
                    // Edge case - component was destroyed immediately,
                    // most likely as a result of a binding initialising
                    run_all(new_on_destroy);
                }
                component.$$.on_mount = [];
            });
        }
        after_update.forEach(add_render_callback);
    }
    function destroy_component(component, detaching) {
        const $$ = component.$$;
        if ($$.fragment !== null) {
            run_all($$.on_destroy);
            $$.fragment && $$.fragment.d(detaching);
            // TODO null out other refs, including component.$$ (but need to
            // preserve final state?)
            $$.on_destroy = $$.fragment = null;
            $$.ctx = [];
        }
    }
    function make_dirty(component, i) {
        if (component.$$.dirty[0] === -1) {
            dirty_components.push(component);
            schedule_update();
            component.$$.dirty.fill(0);
        }
        component.$$.dirty[(i / 31) | 0] |= (1 << (i % 31));
    }
    function init(component, options, instance, create_fragment, not_equal, props, dirty = [-1]) {
        const parent_component = current_component;
        set_current_component(component);
        const $$ = component.$$ = {
            fragment: null,
            ctx: null,
            // state
            props,
            update: noop,
            not_equal,
            bound: blank_object(),
            // lifecycle
            on_mount: [],
            on_destroy: [],
            on_disconnect: [],
            before_update: [],
            after_update: [],
            context: new Map(parent_component ? parent_component.$$.context : options.context || []),
            // everything else
            callbacks: blank_object(),
            dirty,
            skip_bound: false
        };
        let ready = false;
        $$.ctx = instance
            ? instance(component, options.props || {}, (i, ret, ...rest) => {
                const value = rest.length ? rest[0] : ret;
                if ($$.ctx && not_equal($$.ctx[i], $$.ctx[i] = value)) {
                    if (!$$.skip_bound && $$.bound[i])
                        $$.bound[i](value);
                    if (ready)
                        make_dirty(component, i);
                }
                return ret;
            })
            : [];
        $$.update();
        ready = true;
        run_all($$.before_update);
        // `false` as a special case of no DOM component
        $$.fragment = create_fragment ? create_fragment($$.ctx) : false;
        if (options.target) {
            if (options.hydrate) {
                const nodes = children(options.target);
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.l(nodes);
                nodes.forEach(detach);
            }
            else {
                // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
                $$.fragment && $$.fragment.c();
            }
            if (options.intro)
                transition_in(component.$$.fragment);
            mount_component(component, options.target, options.anchor, options.customElement);
            flush();
        }
        set_current_component(parent_component);
    }
    let SvelteElement;
    if (typeof HTMLElement === 'function') {
        SvelteElement = class extends HTMLElement {
            constructor() {
                super();
                this.attachShadow({ mode: 'open' });
            }
            connectedCallback() {
                const { on_mount } = this.$$;
                this.$$.on_disconnect = on_mount.map(run).filter(is_function);
                // @ts-ignore todo: improve typings
                for (const key in this.$$.slotted) {
                    // @ts-ignore todo: improve typings
                    this.appendChild(this.$$.slotted[key]);
                }
            }
            attributeChangedCallback(attr, _oldValue, newValue) {
                this[attr] = newValue;
            }
            disconnectedCallback() {
                run_all(this.$$.on_disconnect);
            }
            $destroy() {
                destroy_component(this, 1);
                this.$destroy = noop;
            }
            $on(type, callback) {
                // TODO should this delegate to addEventListener?
                const callbacks = (this.$$.callbacks[type] || (this.$$.callbacks[type] = []));
                callbacks.push(callback);
                return () => {
                    const index = callbacks.indexOf(callback);
                    if (index !== -1)
                        callbacks.splice(index, 1);
                };
            }
            $set($$props) {
                if (this.$$set && !is_empty($$props)) {
                    this.$$.skip_bound = true;
                    this.$$set($$props);
                    this.$$.skip_bound = false;
                }
            }
        };
    }

    function dispatch_dev(type, detail) {
        document.dispatchEvent(custom_event(type, Object.assign({ version: '3.38.2' }, detail)));
    }
    function append_dev(target, node) {
        dispatch_dev('SvelteDOMInsert', { target, node });
        append(target, node);
    }
    function insert_dev(target, node, anchor) {
        dispatch_dev('SvelteDOMInsert', { target, node, anchor });
        insert(target, node, anchor);
    }
    function detach_dev(node) {
        dispatch_dev('SvelteDOMRemove', { node });
        detach(node);
    }
    function listen_dev(node, event, handler, options, has_prevent_default, has_stop_propagation) {
        const modifiers = options === true ? ['capture'] : options ? Array.from(Object.keys(options)) : [];
        if (has_prevent_default)
            modifiers.push('preventDefault');
        if (has_stop_propagation)
            modifiers.push('stopPropagation');
        dispatch_dev('SvelteDOMAddEventListener', { node, event, handler, modifiers });
        const dispose = listen(node, event, handler, options);
        return () => {
            dispatch_dev('SvelteDOMRemoveEventListener', { node, event, handler, modifiers });
            dispose();
        };
    }
    function attr_dev(node, attribute, value) {
        attr(node, attribute, value);
        if (value == null)
            dispatch_dev('SvelteDOMRemoveAttribute', { node, attribute });
        else
            dispatch_dev('SvelteDOMSetAttribute', { node, attribute, value });
    }
    function set_data_dev(text, data) {
        data = '' + data;
        if (text.wholeText === data)
            return;
        dispatch_dev('SvelteDOMSetData', { node: text, data });
        text.data = data;
    }
    function validate_each_argument(arg) {
        if (typeof arg !== 'string' && !(arg && typeof arg === 'object' && 'length' in arg)) {
            let msg = '{#each} only iterates over array-like objects.';
            if (typeof Symbol === 'function' && arg && Symbol.iterator in arg) {
                msg += ' You can use a spread to convert this iterable into an array.';
            }
            throw new Error(msg);
        }
    }
    function validate_slots(name, slot, keys) {
        for (const slot_key of Object.keys(slot)) {
            if (!~keys.indexOf(slot_key)) {
                console.warn(`<${name}> received an unexpected slot "${slot_key}".`);
            }
        }
    }

    /* src/App.svelte generated by Svelte v3.38.2 */

    const file = "src/App.svelte";

    function get_each_context(ctx, list, i) {
    	const child_ctx = ctx.slice();
    	child_ctx[21] = list[i];
    	child_ctx[23] = i;
    	return child_ctx;
    }

    // (90:0) {#if !visible}
    function create_if_block_3(ctx) {
    	let span;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			span = element("span");
    			span.textContent = "+";
    			attr_dev(span, "class", "icon");
    			add_location(span, file, 90, 1, 2037);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, span, anchor);

    			if (!mounted) {
    				dispose = listen_dev(span, "click", /*clickIcon*/ ctx[9], false, false, false);
    				mounted = true;
    			}
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(span);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_3.name,
    		type: "if",
    		source: "(90:0) {#if !visible}",
    		ctx
    	});

    	return block;
    }

    // (93:0) {#if visible}
    function create_if_block(ctx) {
    	let div;
    	let t;
    	let mounted;
    	let dispose;
    	let each_value = /*comments*/ ctx[0];
    	validate_each_argument(each_value);
    	let each_blocks = [];

    	for (let i = 0; i < each_value.length; i += 1) {
    		each_blocks[i] = create_each_block(get_each_context(ctx, each_value, i));
    	}

    	let if_block = /*addCommentMode*/ ctx[3] && create_if_block_1(ctx);

    	const block = {
    		c: function create() {
    			div = element("div");

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].c();
    			}

    			t = space();
    			if (if_block) if_block.c();
    			attr_dev(div, "class", "post-it");
    			add_location(div, file, 93, 1, 2107);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);

    			for (let i = 0; i < each_blocks.length; i += 1) {
    				each_blocks[i].m(div, null);
    			}

    			append_dev(div, t);
    			if (if_block) if_block.m(div, null);
    			/*div_binding*/ ctx[20](div);

    			if (!mounted) {
    				dispose = listen_dev(div, "click", /*clickPostIt*/ ctx[8], false, false, false);
    				mounted = true;
    			}
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*comments, clickDelete, readonly*/ 4099) {
    				each_value = /*comments*/ ctx[0];
    				validate_each_argument(each_value);
    				let i;

    				for (i = 0; i < each_value.length; i += 1) {
    					const child_ctx = get_each_context(ctx, each_value, i);

    					if (each_blocks[i]) {
    						each_blocks[i].p(child_ctx, dirty);
    					} else {
    						each_blocks[i] = create_each_block(child_ctx);
    						each_blocks[i].c();
    						each_blocks[i].m(div, t);
    					}
    				}

    				for (; i < each_blocks.length; i += 1) {
    					each_blocks[i].d(1);
    				}

    				each_blocks.length = each_value.length;
    			}

    			if (/*addCommentMode*/ ctx[3]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block_1(ctx);
    					if_block.c();
    					if_block.m(div, null);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			destroy_each(each_blocks, detaching);
    			if (if_block) if_block.d();
    			/*div_binding*/ ctx[20](null);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block.name,
    		type: "if",
    		source: "(93:0) {#if visible}",
    		ctx
    	});

    	return block;
    }

    // (98:31) {#if !readonly}
    function create_if_block_2(ctx) {
    	let div;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			div = element("div");
    			div.textContent = "X";
    			attr_dev(div, "class", "delete-symbol");
    			add_location(div, file, 97, 47, 2422);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);

    			if (!mounted) {
    				dispose = listen_dev(div, "click", /*clickDelete*/ ctx[12], false, false, false);
    				mounted = true;
    			}
    		},
    		p: noop,
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			mounted = false;
    			dispose();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_2.name,
    		type: "if",
    		source: "(98:31) {#if !readonly}",
    		ctx
    	});

    	return block;
    }

    // (96:2) {#each comments as comment,i}
    function create_each_block(ctx) {
    	let div;
    	let b;
    	let t0_value = /*comment*/ ctx[21].initials + "";
    	let t0;
    	let t1;
    	let b_title_value;
    	let t2;
    	let t3_value = /*comment*/ ctx[21].text + "";
    	let t3;
    	let t4;
    	let div_class_value;
    	let if_block = !/*readonly*/ ctx[1] && create_if_block_2(ctx);

    	const block = {
    		c: function create() {
    			div = element("div");
    			b = element("b");
    			t0 = text(t0_value);
    			t1 = text(":");
    			t2 = space();
    			t3 = text(t3_value);
    			t4 = space();
    			if (if_block) if_block.c();
    			attr_dev(b, "title", b_title_value = /*comment*/ ctx[21].name);
    			add_location(b, file, 96, 117, 2324);

    			attr_dev(div, "class", div_class_value = "one-comment " + (/*comment*/ ctx[21].newComment && /*comment*/ ctx[21].type !== "oldValue"
    			? "delete-comment"
    			: ""));

    			attr_dev(div, "data-num", /*i*/ ctx[23]);
    			add_location(div, file, 96, 3, 2210);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div, anchor);
    			append_dev(div, b);
    			append_dev(b, t0);
    			append_dev(b, t1);
    			append_dev(div, t2);
    			append_dev(div, t3);
    			append_dev(div, t4);
    			if (if_block) if_block.m(div, null);
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*comments*/ 1 && t0_value !== (t0_value = /*comment*/ ctx[21].initials + "")) set_data_dev(t0, t0_value);

    			if (dirty & /*comments*/ 1 && b_title_value !== (b_title_value = /*comment*/ ctx[21].name)) {
    				attr_dev(b, "title", b_title_value);
    			}

    			if (dirty & /*comments*/ 1 && t3_value !== (t3_value = /*comment*/ ctx[21].text + "")) set_data_dev(t3, t3_value);

    			if (!/*readonly*/ ctx[1]) {
    				if (if_block) {
    					if_block.p(ctx, dirty);
    				} else {
    					if_block = create_if_block_2(ctx);
    					if_block.c();
    					if_block.m(div, null);
    				}
    			} else if (if_block) {
    				if_block.d(1);
    				if_block = null;
    			}

    			if (dirty & /*comments*/ 1 && div_class_value !== (div_class_value = "one-comment " + (/*comment*/ ctx[21].newComment && /*comment*/ ctx[21].type !== "oldValue"
    			? "delete-comment"
    			: ""))) {
    				attr_dev(div, "class", div_class_value);
    			}
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div);
    			if (if_block) if_block.d();
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_each_block.name,
    		type: "each",
    		source: "(96:2) {#each comments as comment,i}",
    		ctx
    	});

    	return block;
    }

    // (102:2) {#if addCommentMode}
    function create_if_block_1(ctx) {
    	let div1;
    	let b;
    	let t0_value = /*user*/ ctx[4].initials + "";
    	let t0;
    	let t1;
    	let span;
    	let t2;
    	let div0;
    	let t3_value = /*text*/ ctx[7].send + "";
    	let t3;
    	let mounted;
    	let dispose;

    	const block = {
    		c: function create() {
    			div1 = element("div");
    			b = element("b");
    			t0 = text(t0_value);
    			t1 = text(": ");
    			span = element("span");
    			t2 = space();
    			div0 = element("div");
    			t3 = text(t3_value);
    			add_location(b, file, 102, 24, 2554);
    			attr_dev(span, "contenteditable", "true");
    			attr_dev(span, "class", "add-text");
    			attr_dev(span, "tabindex", "0");
    			add_location(span, file, 102, 48, 2578);
    			attr_dev(div0, "class", "send");
    			add_location(div0, file, 103, 3, 2696);
    			attr_dev(div1, "class", "comment");
    			add_location(div1, file, 102, 3, 2533);
    		},
    		m: function mount(target, anchor) {
    			insert_dev(target, div1, anchor);
    			append_dev(div1, b);
    			append_dev(b, t0);
    			append_dev(b, t1);
    			append_dev(div1, span);
    			/*span_binding*/ ctx[19](span);
    			append_dev(div1, t2);
    			append_dev(div1, div0);
    			append_dev(div0, t3);

    			if (!mounted) {
    				dispose = [
    					listen_dev(span, "keydown", /*handleKeydown*/ ctx[10], false, false, false),
    					listen_dev(div0, "click", /*clickSend*/ ctx[11], false, false, false)
    				];

    				mounted = true;
    			}
    		},
    		p: function update(ctx, dirty) {
    			if (dirty & /*user*/ 16 && t0_value !== (t0_value = /*user*/ ctx[4].initials + "")) set_data_dev(t0, t0_value);
    			if (dirty & /*text*/ 128 && t3_value !== (t3_value = /*text*/ ctx[7].send + "")) set_data_dev(t3, t3_value);
    		},
    		d: function destroy(detaching) {
    			if (detaching) detach_dev(div1);
    			/*span_binding*/ ctx[19](null);
    			mounted = false;
    			run_all(dispose);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_if_block_1.name,
    		type: "if",
    		source: "(102:2) {#if addCommentMode}",
    		ctx
    	});

    	return block;
    }

    function create_fragment(ctx) {
    	let t;
    	let if_block1_anchor;
    	let if_block0 = !/*visible*/ ctx[2] && create_if_block_3(ctx);
    	let if_block1 = /*visible*/ ctx[2] && create_if_block(ctx);

    	const block = {
    		c: function create() {
    			if (if_block0) if_block0.c();
    			t = space();
    			if (if_block1) if_block1.c();
    			if_block1_anchor = empty();
    			this.c = noop;
    		},
    		l: function claim(nodes) {
    			throw new Error("options.hydrate only works if the component was compiled with the `hydratable: true` option");
    		},
    		m: function mount(target, anchor) {
    			if (if_block0) if_block0.m(target, anchor);
    			insert_dev(target, t, anchor);
    			if (if_block1) if_block1.m(target, anchor);
    			insert_dev(target, if_block1_anchor, anchor);
    		},
    		p: function update(ctx, [dirty]) {
    			if (!/*visible*/ ctx[2]) {
    				if (if_block0) {
    					if_block0.p(ctx, dirty);
    				} else {
    					if_block0 = create_if_block_3(ctx);
    					if_block0.c();
    					if_block0.m(t.parentNode, t);
    				}
    			} else if (if_block0) {
    				if_block0.d(1);
    				if_block0 = null;
    			}

    			if (/*visible*/ ctx[2]) {
    				if (if_block1) {
    					if_block1.p(ctx, dirty);
    				} else {
    					if_block1 = create_if_block(ctx);
    					if_block1.c();
    					if_block1.m(if_block1_anchor.parentNode, if_block1_anchor);
    				}
    			} else if (if_block1) {
    				if_block1.d(1);
    				if_block1 = null;
    			}
    		},
    		i: noop,
    		o: noop,
    		d: function destroy(detaching) {
    			if (if_block0) if_block0.d(detaching);
    			if (detaching) detach_dev(t);
    			if (if_block1) if_block1.d(detaching);
    			if (detaching) detach_dev(if_block1_anchor);
    		}
    	};

    	dispatch_dev("SvelteRegisterBlock", {
    		block,
    		id: create_fragment.name,
    		type: "component",
    		source: "",
    		ctx
    	});

    	return block;
    }

    function instance($$self, $$props, $$invalidate) {
    	let { $$slots: slots = {}, $$scope } = $$props;
    	validate_slots("post-it", slots, []);
    	let { comments = [] } = $$props;
    	let { uid = 0 } = $$props;
    	let { initials = "FD" } = $$props;
    	let { name = "flying dog" } = $$props;
    	let { send = "Send" } = $$props;
    	let { readonly = false } = $$props;
    	let visible = false;
    	let addCommentMode = false;
    	let user = {};
    	let postIt;
    	let addText;
    	let text = {};

    	function clickPostIt() {
    		if (readonly) return;

    		if (addCommentMode) {
    			addText.focus();
    			return;
    		} else {
    			$$invalidate(3, addCommentMode = true);
    		}
    	}

    	function clickIcon() {
    		if (readonly) return;
    		$$invalidate(2, visible = true);
    		$$invalidate(3, addCommentMode = true);
    	}

    	function handleKeydown(e) {
    		if (e.keyCode === 27) {
    			$$invalidate(3, addCommentMode = false);
    			$$invalidate(2, visible = false);
    		}
    	}

    	afterUpdate(() => {
    		if (addCommentMode && addText) addText.focus();
    	});

    	function clickSend(e) {
    		$$invalidate(3, addCommentMode = false);
    		addComment(user, addText.innerText, true);
    		e.stopPropagation();
    	}

    	function clickDelete(e) {
    		let num = e.target.parentElement.getAttribute("data-num");
    		deleteComment(num);
    		e.stopPropagation();
    	}

    	function addComment(user, text, newComment = false, type = "") {
    		if (text.length) {
    			comments.push({
    				name: user.name,
    				initials: user.initials,
    				uid: user.uid,
    				text,
    				newComment,
    				type
    			});

    			$$invalidate(0, comments = comments); // weird
    		}
    	}

    	function deleteComment(num) {
    		if (!comments) return;
    		comments.splice(num, 1);
    		$$invalidate(0, comments = comments); // weird        
    		if (!comments.length) $$invalidate(2, visible = false);
    	}

    	onMount(() => {
    		if (comments.length) $$invalidate(2, visible = true);
    		$$invalidate(4, user = { uid, initials, name });
    		$$invalidate(7, text = { send });
    	});

    	const writable_props = ["comments", "uid", "initials", "name", "send", "readonly"];

    	Object.keys($$props).forEach(key => {
    		if (!~writable_props.indexOf(key) && key.slice(0, 2) !== "$$") console.warn(`<post-it> was created with unknown prop '${key}'`);
    	});

    	function span_binding($$value) {
    		binding_callbacks[$$value ? "unshift" : "push"](() => {
    			addText = $$value;
    			$$invalidate(6, addText);
    		});
    	}

    	function div_binding($$value) {
    		binding_callbacks[$$value ? "unshift" : "push"](() => {
    			postIt = $$value;
    			$$invalidate(5, postIt);
    		});
    	}

    	$$self.$$set = $$props => {
    		if ("comments" in $$props) $$invalidate(0, comments = $$props.comments);
    		if ("uid" in $$props) $$invalidate(13, uid = $$props.uid);
    		if ("initials" in $$props) $$invalidate(14, initials = $$props.initials);
    		if ("name" in $$props) $$invalidate(15, name = $$props.name);
    		if ("send" in $$props) $$invalidate(16, send = $$props.send);
    		if ("readonly" in $$props) $$invalidate(1, readonly = $$props.readonly);
    	};

    	$$self.$capture_state = () => ({
    		onMount,
    		afterUpdate,
    		comments,
    		uid,
    		initials,
    		name,
    		send,
    		readonly,
    		visible,
    		addCommentMode,
    		user,
    		postIt,
    		addText,
    		text,
    		clickPostIt,
    		clickIcon,
    		handleKeydown,
    		clickSend,
    		clickDelete,
    		addComment,
    		deleteComment
    	});

    	$$self.$inject_state = $$props => {
    		if ("comments" in $$props) $$invalidate(0, comments = $$props.comments);
    		if ("uid" in $$props) $$invalidate(13, uid = $$props.uid);
    		if ("initials" in $$props) $$invalidate(14, initials = $$props.initials);
    		if ("name" in $$props) $$invalidate(15, name = $$props.name);
    		if ("send" in $$props) $$invalidate(16, send = $$props.send);
    		if ("readonly" in $$props) $$invalidate(1, readonly = $$props.readonly);
    		if ("visible" in $$props) $$invalidate(2, visible = $$props.visible);
    		if ("addCommentMode" in $$props) $$invalidate(3, addCommentMode = $$props.addCommentMode);
    		if ("user" in $$props) $$invalidate(4, user = $$props.user);
    		if ("postIt" in $$props) $$invalidate(5, postIt = $$props.postIt);
    		if ("addText" in $$props) $$invalidate(6, addText = $$props.addText);
    		if ("text" in $$props) $$invalidate(7, text = $$props.text);
    	};

    	if ($$props && "$$inject" in $$props) {
    		$$self.$inject_state($$props.$$inject);
    	}

    	return [
    		comments,
    		readonly,
    		visible,
    		addCommentMode,
    		user,
    		postIt,
    		addText,
    		text,
    		clickPostIt,
    		clickIcon,
    		handleKeydown,
    		clickSend,
    		clickDelete,
    		uid,
    		initials,
    		name,
    		send,
    		addComment,
    		deleteComment,
    		span_binding,
    		div_binding
    	];
    }

    class App extends SvelteElement {
    	constructor(options) {
    		super();
    		this.shadowRoot.innerHTML = `<style>.icon{font-family:'Kalam', Helvetica, Arial, sans-serif;font-size:14px;width:14px;height:14px;line-height:16px;padding:0;color:var(--color, #000);background:var(--background, #F5F888);box-shadow:0px 3px 3px -1px rgb(0 0 0 / 26%);margin-left:10px;margin-top:5px;text-align:center;cursor:pointer;display:inline-block;visibility:var(--icon-visibility, visible)}.icon:hover{font-weight:bold;cursor:pointer}.post-it{font-family:'Kalam', Helvetica, Arial, sans-serif;font-size:var(--font-size, 12px);padding:10px;color:var(--color, #000);background:var(--background, #F5F888);max-width:var(--width, 230px);border-bottom-left-radius:3%;border-bottom-right-radius:2%;box-shadow:0px 3px 10px -1px rgb(0 0 0  / 16%);transform:rotate(-2deg);cursor:pointer}.post-it:hover{transform:rotate(0deg)}.one-comment{position:relative}.send{background:var(--button-bgcolor, #258ecd);color:var(--button-color, white);text-align:center;padding:5px;margin-top:20px;cursor:pointer}.send:hover{box-shadow:0 5px 11px 0 rgba(0, 0, 0, 0.18), 0 4px 15px 0 rgba(0, 0, 0, 0.15)}.add-text{outline:0px solid transparent;margin-left:5px}.delete-symbol{position:absolute;right:0;top:0;display:none;border:1px solid red;border-radius:100%;width:16px;height:16px;text-align:center;color:red}.delete-comment:hover .delete-symbol{display:block}</style>`;

    		init(
    			this,
    			{
    				target: this.shadowRoot,
    				props: attribute_to_object(this.attributes),
    				customElement: true
    			},
    			instance,
    			create_fragment,
    			safe_not_equal,
    			{
    				comments: 0,
    				uid: 13,
    				initials: 14,
    				name: 15,
    				send: 16,
    				readonly: 1,
    				addComment: 17,
    				deleteComment: 18
    			}
    		);

    		if (options) {
    			if (options.target) {
    				insert_dev(options.target, this, options.anchor);
    			}

    			if (options.props) {
    				this.$set(options.props);
    				flush();
    			}
    		}
    	}

    	static get observedAttributes() {
    		return [
    			"comments",
    			"uid",
    			"initials",
    			"name",
    			"send",
    			"readonly",
    			"addComment",
    			"deleteComment"
    		];
    	}

    	get comments() {
    		return this.$$.ctx[0];
    	}

    	set comments(comments) {
    		this.$set({ comments });
    		flush();
    	}

    	get uid() {
    		return this.$$.ctx[13];
    	}

    	set uid(uid) {
    		this.$set({ uid });
    		flush();
    	}

    	get initials() {
    		return this.$$.ctx[14];
    	}

    	set initials(initials) {
    		this.$set({ initials });
    		flush();
    	}

    	get name() {
    		return this.$$.ctx[15];
    	}

    	set name(name) {
    		this.$set({ name });
    		flush();
    	}

    	get send() {
    		return this.$$.ctx[16];
    	}

    	set send(send) {
    		this.$set({ send });
    		flush();
    	}

    	get readonly() {
    		return this.$$.ctx[1];
    	}

    	set readonly(readonly) {
    		this.$set({ readonly });
    		flush();
    	}

    	get addComment() {
    		return this.$$.ctx[17];
    	}

    	set addComment(value) {
    		throw new Error("<post-it>: Cannot set read-only property 'addComment'");
    	}

    	get deleteComment() {
    		return this.$$.ctx[18];
    	}

    	set deleteComment(value) {
    		throw new Error("<post-it>: Cannot set read-only property 'deleteComment'");
    	}
    }

    customElements.define("post-it", App);

    const app = new App({
    	target: document.body,
    	props: {
    		name: 'world'
    	}
    });

    return app;

}());
//# sourceMappingURL=post-it.js.map
